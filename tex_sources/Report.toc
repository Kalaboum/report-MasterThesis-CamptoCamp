\contentsline {section}{\numberline {1}Introduction}{4}{section.1}
\contentsline {section}{\numberline {2}Fundamentals of Cartography and web mapping}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Projections}{5}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Theoretical background}{5}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Applications of differential geometry to cartography}{6}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}GPS coordinates}{7}{subsubsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.4}Web Mercator}{7}{subsubsection.2.1.4}
\contentsline {subsubsection}{\numberline {2.1.5}Reprojection}{8}{subsubsection.2.1.5}
\contentsline {subsubsection}{\numberline {2.1.6}From parameterization to pixel coordinates}{8}{subsubsection.2.1.6}
\contentsline {subsubsection}{\numberline {2.1.7}Dirty reprojectors}{8}{subsubsection.2.1.7}
\contentsline {paragraph}{Theory}{8}{section*.2}
\contentsline {paragraph}{Application}{9}{section*.3}
\contentsline {subsubsection}{\numberline {2.1.8}Projections code}{10}{subsubsection.2.1.8}
\contentsline {subsection}{\numberline {2.2}Standard specifications of web services}{10}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Raster and vector tiles}{11}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Raster tiles}{11}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Vector tiles}{12}{subsubsection.2.3.2}
\contentsline {subsubsection}{\numberline {2.3.3}Vector tiles format}{12}{subsubsection.2.3.3}
\contentsline {subsubsection}{\numberline {2.3.4}Protocol buffer format}{12}{subsubsection.2.3.4}
\contentsline {subsubsection}{\numberline {2.3.5}Mapbox vector tile specification}{12}{subsubsection.2.3.5}
\contentsline {subsection}{\numberline {2.4}Label placement}{13}{subsection.2.4}
\contentsline {section}{\numberline {3}Performance evaluation of renderers}{17}{section.3}
\contentsline {subsection}{\numberline {3.1}Introduction}{17}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Quality of Experience (QoE)}{17}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}State of the art}{17}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Using correlates of QoE as metrics}{18}{subsubsection.3.2.2}
\contentsline {subsection}{\numberline {3.3}Renderers}{18}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Methods}{19}{subsection.3.4}
\contentsline {subsubsection}{\numberline {3.4.1}Introduction}{19}{subsubsection.3.4.1}
\contentsline {subsubsection}{\numberline {3.4.2}Definitions}{19}{subsubsection.3.4.2}
\contentsline {subsubsection}{\numberline {3.4.3}Mitigating the bias}{20}{subsubsection.3.4.3}
\contentsline {subsubsection}{\numberline {3.4.4}Navigation paths}{21}{subsubsection.3.4.4}
\contentsline {subsubsection}{\numberline {3.4.5}Tiles used}{22}{subsubsection.3.4.5}
\contentsline {subsubsection}{\numberline {3.4.6}Styles}{22}{subsubsection.3.4.6}
\contentsline {subsubsection}{\numberline {3.4.7}Hardware and software used}{24}{subsubsection.3.4.7}
\contentsline {subsection}{\numberline {3.5}Results}{24}{subsection.3.5}
\contentsline {subsubsection}{\numberline {3.5.1}Comparing versions of openlayers}{24}{subsubsection.3.5.1}
\contentsline {subsubsection}{\numberline {3.5.2}Comparing versions of mapbox-gl and other tests}{26}{subsubsection.3.5.2}
\contentsline {section}{\numberline {4}Public visualization of transports using vector tiles}{30}{section.4}
\contentsline {subsection}{\numberline {4.1}Introduction}{30}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}General Transit Feed Specification (GTFS)}{30}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Importing Data}{32}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Clipping}{33}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Styling}{34}{subsection.4.5}
\contentsline {section}{\numberline {5}Conclusion and future work}{36}{section.5}
