\section{Performance evaluation of renderers}

\subsection{Introduction}

In this section, an application comparing the performance of renderers in terms of Frames Per Second (FPS) is presented. The application is available on the following URL: \url{https://github.com/camptocamp/eval-perf-ol-mapbox}. The next subsection describes our reflection on the criteria which make a good renderer based on previous literature, and the reasons why we made the arbitrary choice of choosing FPS as an evaluation metric.

\input{QoE}

\subsection{Renderers} 
There are three main open-source libraries which render maps in a browser:
\begin{itemize}
\item \href{https://www.mapbox.com/mapbox-gl-js/api/}{mapbox-gl}
\item \href{http://openlayers.org/}{openlayers}
\item \href{https://leafletjs.com/}{leaflet}
\end{itemize}
 Of these three, only mapbox-gl does full vector rendering. Leaflet has no built-in for vector tiles but there is an experimental tool: \href{https://github.com/mapbox/mapbox-gl-leaflet}{mapbox-gl-leaflet}, not actively supported by mapbox, which allows to draw a mapbox layer on top of a leaflet map.
Openlayers has a hybrid-mode where vector point data are rendered as vector, and lines and polygons are rasterized. This mode supports styles from mapbox using an external tool: \href{https://github.com/boundlessgeo/ol-mapbox-style}{ol-mapbox-style}.
Openlayers also has a full vector renderer mode but it has seen less intensive development for a few years, so the performance are not state-of-the-art and ol-mapbox-style does not support it.
\subsection{Methods} 
 
 \subsubsection{Introduction}
 
 To assess the performance of the renderers, we used Selenium, a tool of test automation user interface. This allowed us to have a browser in puppet mode to allow javascript code injection. We computed metrics over a set of scenarii where we mimicked user interaction with a webmap. This section describes how this automated testing has been implemented as well as the theoretical motivation behind it.
 
 \subsubsection{Definitions}
 FPS can be calculated in different ways. \textit{Stricto sensu}, it is the number of frames in one second. It is also possible to extrapolate the instantaneous FPS of a frame $f$: $iFPS(f)$ which can be calculated as :
\begin{equation}
iFPS(f) = \dfrac{1}{d(f)} 
\label{eq:iFPS}
\end{equation}
Where $d(f)$ is the duration of the frame in seconds. In practice the instantaneous FPS of frames vary too much to be of any use. This is why implementations of FPS counter use the average of iFPS on $n$ frames:
\begin{equation}
aFPS(n, F) = \dfrac{n}{\sum_{i = 1}^n d(F_i) }
\end{equation} 
Where $F$ is the set of frames passed. We define the FPS of a run, $FPS(r)$, as:
\begin{equation}
FPS(r) = \dfrac{n(r)}{d(r)}
\label{eq:FPS-run}
\end{equation}
Where $n(r)$ is the number of frames of the run and $d(r)$ is the duration of the run. $FPS(r)$ is equivalent to $aFPS(n, F)$ if $n = n(r)$ and $F$ is the set of frames of the run.

Animation in browser is done via calls to requestAnimationFrame \cite{requestAnimationFrame} which takes a function as callback. The browser will schedule this function at the next frame. If the callback function itself calls requestAnimationFrame, the visual content is repeatedly updated. Most browsers use a default cap of 60 FPS. When the callback function takes too much time to compute, there is a drop in the FPS.
The duration of a frame can be modelized as:
 \begin{equation}
D = \dfrac{W} {E \cdot C } + N + O
\label{eq:duration}
 \end{equation}
 Where 
 \begin{itemize}
 \item $W$ is the workload needed to render the visual content
 \item $E$ is the efficiency of the renderer, which is what we would like to evaluate
 \item $C$ is the computational resources allocated to the browser
 \item $N$ is the delay due to the network, a renderer may wait on tiles before doing its rendering tasks
 \item $O$ represents all the other tasks performed by the browser, such as garbage collecting, etc... This can be an important part of D and vary greatly from one browser to another.
 \end{itemize}
 
For each frame we captured the duration of the activity of the renderer (render time). This render time is 0 when the renderer does not need to repaint the content. The frame for which this render time is maximum, is called the slowest frame. We call the duration when the renderer is active in this frame as the render time of the slowest frame of a run, it will be used as a metric to evaluate performance (see section \ref{sec:metrics}).

 \subsubsection{Mitigating the bias}
 Comparing two different renderers is hard, because of the many biases that can be introduced during the measure.
 Here we are making the strong assumption that in equation \ref{eq:duration}, the frame duration is directly correlated to efficiency of the renderer, because we would like to have FPS as reliable indicators of efficiency of the renderers. However $D$ depends on several other factors. We can mitigate the variance of those factors across our experiments using the following methods:\\
Concerning $N$, the network delay, we saw the tiles were cached by the browser after one run, if the first run is an outlier, it is discarded from the sample set. To minimize the impact of $C$,  computational power, on the performance, we used the same hardware and operating system to run the experiments. \\
%%TODO find the specs
However the operating system decides how to allocate the computational resources to the processes.  The experiments were launched without other user-launched processes. $C$ cannot be considered as a constant, but as a random experiment having low variance.\\
To reduce the variance of $O$, we used a single browser and did several runs for each experiment.\\
To compare the efficiency of the renderers, they must be tested on tasks of equal difficulty. We used the same style in each experiment. The style is edited with maputnik, an open-source program which allows to produce mapbox-gl-js styles. The library ol-mapbox-style from Boundless is used to have the same visual content from openlayers and from mapbox-gl-js.\\
Using Selenium we designed the same navigation path, a combination of pans, zooms and pauses to ensure an identical visual content.

\subsubsection{Navigation paths}

We first used the selenium actions to simulate mouse movements and clicks to generate a reproducible scenario of navigation. This method has several issues. \\
A minor one was that at the time of the implementation, the  \href{https://w3c.github.io/webdriver/}{W3C specification of webdriver} was enforced by Firefox and not by Chrome. This required that we implement two versions for each navigation path, and would introduce a bias if we wanted to compare the performance of the browsers. \\
The major issue came from the different ways that mapbox-gl and openlayers handled panning. Using default options of animation for the pan, a drag of 200 pixels moved the map further away in openlayers than in mapbox. \\
To address this issue, we dropped the idea of simulating mouse movements and injected javascript code in the browser using Selenium. The code injected calls built-in functions from the webmap libraries which animate a transition in the map (map.panBy() and map.easeTo() from mapbox-gl, and view.animate() from openlayers).
Those functions take an easing function $f$, where $f(0) = 0$, and $f(1) = 1$ and a duration $d$ as parameters. Those parameters allow to control the velocity of the transition between two views of the map. Using the same parameters for the two renderers enforce that the visual content is the same. As a drawback, we cannot use the same implementation to compare the performance of another renderer than mapbox-gl and openlayers.

\subsubsection{Tiles used}

We used tiles from \href{https://openmaptiles.com/}{openmaptiles}. There was a discussion about whether to download the set of tiles for Switzerland and serve them from localhost, or directly request from openmaptiles servers. Since the navigator caches the tiles we settled for the former. The tiles used in the scenarii are those from the west part of Switzerland with a range of zoom from 9 to 14. The size of the tiles vary from 15 KB to 150 KB.

\subsubsection{Styles}

We used three different styles designed with Maputnik for the experiments. A very light simple style showing only the roads in grey, and two styles derived from the OSM-liberty style provided by Lukas Martinelli, the creator of Maputnik. These two last styles: medium and heavy, are complete styles. The medium style is a bit simplified and does not show road labels. An example of each different style is shown on Figure~\ref{fig:styles}

\newgeometry{left=0cm, right=0cm, top=0cm, bottom=0cm}
\begin{figure}
\bgroup
\def\arraystretch{2.5}
\setlength\tabcolsep{0px}
\begin{tabular}{cc}
  \includegraphics[center, scale=0.5]{Report-mapbox-basic-cropped} &
  \includegraphics[center, scale=0.5]{Report-medium-style-cropped} \\
   \includegraphics[center, scale=0.5]{Report-heavy-style-cropped} &
  \end{tabular}
\egroup

\caption{basic style top left, medium style top right, heavy style bottom left}
\label{fig:styles}
\end{figure}

\restoregeometry

\subsubsection{Hardware and software used}

All experiments were run on the same laptop, a lenovo T470p. The specifications are on Table \ref{tab:specs}. For the software, see Table \ref{tab:softspecs}.

\begin{table}[h]
\centering
\begin{tabular}{|c|c|}
\hline
CPU & Intel(R) Core(TM) i5-7440HQ CPU @ 2.80GHz \\
\hline
GPU & Intel® HD Graphics 630 (Kaby Lake GT2) \\
\hline 
RAM & 16GB DDR4 \\
\hline
\end{tabular}
\caption{Hardware specifications}
\label{tab:specs}
\end{table}

\begin{table}[h]
\centering
\begin{tabular}{|c|c|c|}
\hline
Type of software & software used & version \\
\hhline{|=|=|=|}
OS & Ubuntu &16.04 LTS \\
\hline
web navigator & Chrome & 64 \\
\hline 
web driver & chromedriver & 2.37 \\
\hline
Automation test ui & Selenium & 4.0.0-alpha.1 \\
\hline
\end{tabular}
\caption{Software specifications}
\label{tab:softspecs}
\end{table}


\subsection{Results}

\subsubsection{Comparing versions of openlayers}
We used two different navigation paths, a short one of 13 seconds and a longer one of approximatly 40 seconds. Both combined zooms and drags, the former with less time for the animation. We expected a better performance in FPS for the longer scenario, because it is smoother. The performance was compared across four different steps of development of openlayers and ol-mapbox-style during this last year. For each style, each navigation path and each step of development, an experiment was run, except for the older versions of openlayers which do not support the heavy style. Each experiment consisted of 50 runs. A typical run, with the scenario of 13 seconds, containing the instantaneous FPS (cf. equation \ref{eq:iFPS}) can be seen on Figure~\ref{fig:RunSVG}. The irregularites in the instantaneous FPS are the reason why we used the slowest render time of a frame as a metric and not the variance in the FPS which is deemd noisy. This noise can be explained by the fact that the function used to measure FPS is not guaranteed to be called at the beginning of the frame. Another reason lies in the noise added by browsers in functions used to capturate performance, in order to avoid timing attacks such as \href{https://meltdownattack.com/}{Meltdown and Spectre}.

\begin{figure}
\centering
\includesvg[width=480px]{RunSVG}
\centering
\caption{Run of openlayers v4.6.4, with the basic style. \\
Each blue rect is a different frame, width of rectangles represents their duration, y axis represents the instantaneous FPS, violet rectangles show when the renderer is working}
\label{fig:RunSVG}
\end{figure}

We computed statistics on the FPS of runs as defined in equation \ref{eq:FPS-run}. The runs whose FPS lie above the third quartile plus 1.5 times the Interquartile range or below the first quartile minus 1.5 times the Interquartile range were considered as outliers and automatically rejected. No experiment had more than five outliers. The results of these statistics can be seen on Table~\ref{tab:fps-results}.



\begin{table}
\begin{tabular}{|c|c|c|c|c|}
\hline
Date of release & June 2017 & September 2017 & January 2018 & July 2018 \\
\hline
openlayers version & 4.2.0 & 4.3.2 & 4.6.4 & 5.0.3 \\
\hline
ol-mapbox-style version & 2.5.0 & 2.6.5 & 2.10.1 & 3.0.1 \\
\hline
Column in Table \ref{tab:fps-results} & A & B & C & D \\
\hline
\end{tabular}
\caption{versions of openlayers and ol-mapbox-style used}
\label{tab:ol-versions}
\end{table}

The results show that the slow scenario leads to better FPS, as it was expected since this scenario involves more pauses where the renderer is idle. \\
There is no significant improvement of FPS  during the last steps of development of openlayers. One of the reasons is that some improvements aim at improving the visual quality of the rendering. One of those improvements consists of a reduction of label collisions between ol-mapbox-style v2.6.5 and v2.10.1, reducing the workload for the newer versions. This explains the augmentation of FPS for the medium style. \\
In openlayers 5.0.3, the animation for the zoom has changed to make it smoother. This may explain the little drop of FPS on each case compared to the previous version. We designed a scenario without zoom  events to test this hypothesis but the results were inconclusive, the july's version still performed a bit worse than january's one. This does not allow us to prove or disprove our hypothesis that the difference in FPS is due to the amelioration of the zoom animation.



\newcolumntype{C}{ >{\centering\arraybackslash} m{7cm} }
\newcolumntype{D}{ >{\centering\arraybackslash} m{1.5cm} }
\setlength\extrarowheight{5pt}
\begin{table}
\centering
\fontsize{6}{10}\selectfont
\begin{tabular}{|D|C|C|}
\hline
{\normalsize style } & {\normalsize 13 seconds path} & {\normalsize slow scenario} \\
\hline
{\normalsize basic } & \includesvg[width=200px]{Basic5sec} & \includesvg[width=200px]{BasicSlow} \\
\hline
{\normalsize medium } & \includesvg[width=200px]{Medium5sec} & \includesvg[width=200px]{MediumSlow} \\
\hline
{\normalsize heavy } & \includesvg[width=200px]{Heavy5sec} & \includesvg[width=200px]{HeavySlow} \\
\hline
\end{tabular}
\caption{FPS of experiments}
\normalsize{The row indicates the style used, the column shows the path followed by the map. Each cell shows the comparison of different versions of openlayers according to Table \ref{tab:ol-versions}. Box plots show the minimum, first quartile, median, third quartile and maximum of FPS of the different runs. }
\label{tab:fps-results}
\end{table}

For the same experiments, the statistics on the render time of the slowest frame were computed, the results are shown on Table \ref{tab:render-results}. The results are less significant than those with the fps because of their variance. The renderers perform worse on the heavier styles. Without doing the precise calculations, we see an inverse correlation between render time and FPS as expected, with the notable exception of the heavier style with the slow scenario, where the version of July 2018 seems to perform better (lower render times mean better performance) than the version of January 2018. 

\newcolumntype{C}{ >{\centering\arraybackslash} m{7cm} }
\newcolumntype{D}{ >{\centering\arraybackslash} m{1.5cm} }
\setlength\extrarowheight{5pt}
\begin{table}
\centering
\fontsize{6}{10}\selectfont
\begin{tabular}{|D|C|C|}
\hline
{\normalsize style } & {\normalsize 13 seconds path} & {\normalsize slow scenario} \\
\hline
{\normalsize basic } & \includesvg[width=200px]{RenderBasic5sec} & \includesvg[width=200px]{RenderBasicSlow} \\
\hline
{\normalsize medium } & \includesvg[width=200px]{RenderMedium5sec} & \includesvg[width=200px]{RenderMediumSlow} \\
\hline
{\normalsize heavy } & \includesvg[width=200px]{RenderHeavy5sec} & \includesvg[width=200px]{RenderHeavySlow} \\
\hline
\end{tabular}
\caption{Max render time of experiments}
\normalsize{Same format as Table \ref{tab:fps-results} except that the y axis shows the render time of the slowest frame in a run in milliseconds.}
\label{tab:render-results}
\end{table}

\subsubsection{Comparing versions of mapbox-gl and other tests}

The same experiments were conducted for last year versions of mapbox-gl. The results showed that mapbox-gl was almost constantly at 60 FPS whatever the scenario and the style. This is one of the reasons we chosed render time of the slowest frame as a metric. The statistics on the render time of the slowest frame shows there is no significant difference of performance for the  versions tested (see Figure \ref{fig:mapbox-results}).
\begin{figure}
\centering
\includesvg[width=480px]{RenderMapbox}
\centering
\caption{Comparison of different versions of Mapbox (respectively July 2017, January 2018 and July 2018)}
\label{fig:mapbox-results}
\end{figure}

We compared the results of chrome and firefox with the short scenario and the heavy style using mapbox-gl 0.47.0. The results showed a significant decrease in the FPS for firefox, 58 FPS for chrome and 48 FPS for firefox (median of the experiments). The same setup for openlayers 5.0.3 also showed a better performance of chrome: 35 FPS against 13 FPS. \\