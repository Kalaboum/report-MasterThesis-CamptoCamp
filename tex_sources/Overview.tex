\subsection{Standard specifications of web services}

Web Map Tiling Service (WMTS) is a specification published by the Open Geospatial Consortium in 2010. Whereas Web Map Service (WMS) delivers one image (i.e raster) per request corresponding to the bounding box of the viewer, WMTS divides the content in multiple squares called tiles. The client selects the tiles needed to display the content. These two different approaches bring significant changes on the mode of operation of the server: \\
\begin{itemize}
\item In WMS, the base map data is not stored in raster format (except for satellite images) since it would be too voluminous. The server needs to generate an image from the data, which is a time-consuming process. \\
\item With WMTS, a server can pre-compute tiles and thus needs more storage, but response time are shorter. Furthermore, tile caching can be distributed, leading to a solution which scales with the number of users. \\
\end{itemize}
\subsection{Raster and vector tiles}

\subsubsection{Raster tiles}
\label{sec:raster}
%TODO show vector vs raster maps -> blur while zooming
Raster tiles are small images, usually of size 256x256 pixels. Tile rendering is done at the server side. The client needs to know which tiles to query for his current viewport and where on the screen. \\
WMTS is often used with the convention typically called  ``slippy map tilenames''. In this convention tiles have an identifier $z/x/y$ where $z$ stands for zoom level, $x$ is the tile coordinate along the horizontal axis, $y$ is the tile coordinate along the vertical axis. The tile $z/0/0$ is located at the top left corner for each zoom level $z$. At zoom $z$ the entire world is represented by a square of $2^{z}$ by $2^z$ tiles. Caching the tiles for the entire world at zoom 20 would require $2^{40}$ times the size of an image, about 5 petabytes\cite{osmtiles}.\\

\subsubsection{Vector tiles}
Vector tiles do not provide directly images, they contains geometries (points, lines, polygons) and properties of these geometries (name of the road, type of land surface, etc...). The part of the workload concerning the rendering is moved from the server to the client. 
Tiling is usually done in the same way as with raster tiles. However since vectorized content can be scaled without appearing blurry, map can be displayed at any zoom level between two tile zoom level. This allows seamless zooming, rotating and tilting. \\

\subsubsection{Vector tiles format}
Whereas raster tiles have a  typical image format (.jpeg, .png, etc...,) vector tiles are encoded in binary data when sent over the network to reduce bandwidth consumption. There are many possible ways to serialize data and many specifications were created in the last ten years.  Mapbox published in 2014 a \href{https://github.com/mapbox/vector-tile-spec/tree/master/2.1}{specification for vector tiles} \cite{mvt-spec} using the google protocol buffer format (protobuf). This format was then supported by Esri, the dominant geospatial software maker, and became a \textit{de facto} standard \cite{wiki:vectortiles}.
Google use their own vector tiles for Google Maps but it is not an open format.

\subsubsection{Protocol buffer format}
Protobuf is a way to serialize structured data which is more size efficient and faster to encode/decode than XML\cite{web:pbf}. Protobuf files can vary greatly depending on their content, a file \texttt{.proto} describes the data. Mapbox specifies a \texttt{.proto} file for the vector tiles as well as a guide to interpret the messages in the \texttt{.pbf} files. According to best practice a \texttt{.pbf} using mapbox vector tile specification should have the extension \texttt{.mvt} to avoid confusion. Many tools used to generate vector tiles still keep the extension \texttt{.pbf}. \\

\subsubsection{Mapbox vector tile specification}
\label{section:vector-tile-spec}
This specification represents content in a vectorized way allowing over-zooming and pixel-perfect rendering on HiDPI screen.
 Simple geometries (polygons, linestrings and points) are represented on a square grid of size 4096, which is an arbitrary choice. The sides of this square is thus 16 times the side of the square in the screen (in number of pixels, remember that a raster tile is usually 256x256 pixels). Remark that in this specification, tiles have no information about the projection used.\\
A Caveat of the vector tiles is the level of detail and number of features one put in a vector tile. Vector tiles that are large in terms of memory space need more time to download and impact the latency of the webmap. \\
Usually the protobuf files are merged in one \texttt{.mbtiles} file, which is basically a sqlite database allowing queries on the vector tiles.

\subsection{Label placement}

Label placement is a hard and fundamental topic in cartography. Many rules exist on a great variety of subjects such as type color, letter spacing, clear graphic association, orientation, etc.. to specify what makes a map legible and easy to use. Imhof (1975) \cite{imhof1975positioning} states many rules and is acknowledged to be a reference in the domain. 
When cartography was not digital, labels were drawn on a transparent layer laid over the map to check the positioning and coherence of the label. The cartographer could then make adjustements before writing the labels on the map. 
In todays raster maps, this notion of layers is kept, a final map is generally the result of multiple layers drawn one upon eachother. For the published maps of swisstopo, the official maps of Switzerland, the labels are placed automatically and adjusted manually when there are artifacts. 
This time-consuming task is made easier with the automatic initial placement of the labels but is still a delicate subject.
In vector maps, this subject is even more complicated. The literature on label placements assumes that the rotation and the zoom of the map are constants. Whereas raster maps can be considered as multiple maps of constant zoom and rotation, this is not anymore the case with vector maps, where zooming is continuous.  
Been et al. (2016) \cite{been2006dynamic} establishes four requirements for dynamic labeling in web cartography:
 \begin{enumerate}
    \item Labels should not disappear when zooming in or appear when zooming out.
    \item Labels should not disappear or appear when panning except when sliding out of view.
    \item Labels should not jump around, but instead should be anchored.
    \item Label placement should be deterministic, no matter how you got to the current view.
\end{enumerate}.
 
We present some cases of dynamic labeling. On Figure~\ref{fig:klokan-notilt} there is an elegant map with a sober style (taken from \url{http://openmaptiles.org/styles}, the site that evolved from osm2vectortiles.org presented in Martinelli and Roth, 2015 \cite{martinelli2015vector}). In this map, there are two different types of label: point-attached labels and line-attached labels. There is no obvious issue with this map and the label are legible with the help of halos. When the same map is tilted as in Figure~\ref{fig:klokan-tilt}, the point-attached labels face us and are still legible. However the line-attached labels are projected on the street and most of them are not legible anymore.

In the example of Figure~\ref{fig:opensciencemap1} coming from \url{http://opensciencemap.org}, they use their own \href{https://github.com/opensciencemap/vtm}{webgl library} for rendering. We can see conflicts between labels, and that the map is more label-heavy than recommended. 
The following Figure~\ref{fig:opensciencemap2} is almost the same, but rotated a bit more. These two figures illustrate well the issue of ``flickering'' which can arise when rotating a map that is tilted. Having labels constantly appearing and disappearing, while the remaining content is stable, should be avoided.
The mapbox style ``light'' used in Figure~\ref{fig:mapbox-labels} shows a good example of labels correctly positioned, and not projected on the street. This behaviour can be parametrized using the \href{https://www.mapbox.com/mapbox-gl-js/style-spec/\#layout-symbol-text-pitch-alignment}{mapbox-style layout property} text-pitch-alignment.
\newgeometry{left=0cm, right=0cm, top=0cm, bottom=0cm}

\begin{figure}
  \includegraphics[center]{klokantech-notilt}
  \caption{Map using mapbox-gl from \href{http://openmaptiles.org}{openmaptiles}}
  \label{fig:klokan-notilt}
\end{figure}
\begin{figure}
  \includegraphics[center]{klokantech-tilt}
  \caption{The same map as in Figure~\ref{fig:klokan-notilt} but tilted}
  \label{fig:klokan-tilt}
  \end{figure}
  
  \begin{figure}
  \includegraphics[center]{opensciencemap-tilt1}
  \caption{map from \href{http://www.opensciencemap.org/map/}{opensciencemap}}
  \label{fig:opensciencemap1}
\end{figure}
\begin{figure}
  \includegraphics[center]{opensciencemap-tilt2}
  \caption{The same map as in Figure~\ref{fig:opensciencemap1} but rotated a bit more}
  \label{fig:opensciencemap2}
\end{figure}


\begin{figure}
  \includegraphics[center]{mapbox-labels}
  \caption{Sample of a map using mapbox-style ``light''}
  \label{fig:mapbox-labels}
\end{figure}

\restoregeometry