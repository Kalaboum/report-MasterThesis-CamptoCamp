\section{Fundamentals of Cartography and web mapping}

%%\subsection{Introduction}
%Cartography is a science whose importance did not stop growing since the discovery of America. At the epoch of great maritime travel, orientation needed expensive tools and non-trivial technical knowledge and abilities. Nowadays, locating oneself is easier with the help of Global Positioning Systems (GPS) and Web-Geographic Information Systems (GIS).
%More than a fourth of the world's poputation posses a smartphone \cite{statista_phones} in which there is a GPS. Accurate positioning and fast pathfinding is available to anyone.

\subsection{Projections}


\subsubsection{Theoretical background}

A world map is a visualization of the surface of the Earth on a two-dimensional plane. The process of going from the surface of a three-dimensional object (the Earth) to a surface is called a map projection. The visual aspect of a world map depends on its projection. \\
The following definitions and theorems of differential geometry come from Floater and Hormann (2005) \cite{floater2005surface} with some changes in notation for simplicity reasons.
\begin{definition}
A map projection is a parameterization of the surface of the Earth to the plane. A vector $\mathbf{x} \in \mathbb{R}^3$ on a surface can be parametrized by two coordinates $(u, v) \in \mathbb{R}^2$ such that: $\mathbf{x} = (x(u,v), y(u,v), z(u, v))$. Where $x$, $y$ and $z$ are functions from the same domain $\mathbb{D}$ to a scalar.
\end{definition}

\begin{definition}
Such a parameterization is called regular iff the functions $x$, $y$, and $z$ are differentiable at least 3 times and the vectors $\mathbf{x_1} = \dfrac{\partial\mathbf{x}}{\partial u}$ and $\mathbf{x_2} = \dfrac{\partial\mathbf{x}}{\partial v}$ are linearly independent at every point.
\end{definition}

\begin{definition}
The first fundamental form is a matrix defined as such:
\begin{equation}
I = \begin{bmatrix}
    \mathbf{x_1} \cdot \mathbf{x_1} & \mathbf{x_1} \cdot \mathbf{x_2} \\
    \mathbf{x_1} \cdot \mathbf{x_2} & \mathbf{x_2} \cdot \mathbf{x_2}
\end{bmatrix}
\end{equation}
\end{definition}

Here is a concrete example: \\
The surface of a sphere of radius 1 are the sets of points $(x_1, x_2, x_3)$ satisfying $x_1^2 + x_2^2 + x_3^2 = 1$. A parameterization from $(\lambda, \phi) \in [-\pi, \pi] \times [-\pi/2, \pi/2]$ to the surface of the sphere exists, and is given by:
\begin{equation}
\mathbf{x}(\lambda, \phi) = (x_1(\lambda, \phi), x_2(\lambda, \phi), x_3(\lambda, \phi)) = (cos\lambda sin\phi, sin \lambda sin\phi, cos \phi). \\
\label{eq:sphere}
\end{equation}
A parameterization can be seen as a mapping $f$ from a surface $S$ to another surface $S'$. In this example $S = [-\pi, \pi] \times [-\pi/2, \pi/2]$ and $S'$ is the surface of the sphere.
The first fundamental form of this parameterization is : 
\begin{equation}
\begin{bmatrix}
sin^2\phi & 0\\
0 & 1
\end{bmatrix}
\end{equation}

\begin{definition} 
A parameterization is conformal if it keeps the angle of intersecting curves.\\
The angle of intersecting curves is the angle at which their tangent intersect. Every pair of curves in $S$ must intersect with an angle which is the same as the parameterized curves is $S'$.
\end{definition}

\begin{theorem}
A parametrization is conformal iff its first fundamental form is a scalar multiple of the identity.
\end{theorem}

\begin{definition} 
A parameterization is equiareal if it preserves the areas. \\
Every subsurface $A$ in $S$ must have the same area than $f(A)$ which is a subsurface of $S'$. 
\end{definition}

\begin{theorem}
A parameterization is equiareal iff the determinant of the first fundamental form is equal to 1.
\end{theorem}

\begin{definition} 
A parameterization is isometric if it is conformal and equiareal
\end{definition}

Isometric parameterizations are ideal, but rarely possible in practice.

\subsubsection{Applications of differential geometry to cartography}

\begin{theorem}
There is no isometric projection from the surface of a sphere to a plane
\end{theorem}
For a detailed proof, see the article from Robinson (2006) \cite{robinson2006sphere}. It has to do with the sphere not being a developable surface. The reader who is interested by rigorous proofs and deeper knowledge of these concepts is kindly addressed to Michel Berger's book \cite{berger2012panoramic}. \\
This theorem is also true for the ellipsoid, and any shape that is a reasonable approximation of the surface of the Earth. 
Map projections first assume a shape of the Earth which can vary, because the exact shape of the Earth is a mathematical object too complex to be of any practical use. The standard \href{https://en.wikipedia.org/wiki/World_Geodetic_System#WGS84}{WGS84} defines the length of the semi-axis of the ellipsoid (or oblate spheroid of revolution) which serves as an approximation of the shape of the Earth. \\
From this theorem, it follows that a map projection would induce distortion either in the angles or in the area. This is one of the reasons why there exists many projections, because none is ideal. There are also historical or political reasons. The most known projection is called the Mercator Projection, it places Europe at the center and is non-equiareal. In this projection, places further from the equator look bigger, such as Groenland or the USA for instance.

\subsubsection{GPS coordinates}

A point on the Earth using a GPS consists of two coordinates: latitude and longitude. Latitude represents the position on the South-North axis, being 90\degree  at North Pole, 0\degree at the equator, and -90\degree at the South Pole. It corresponds to $\phi$ in our above example of the parameterization of a sphere (see equation~\ref{eq:sphere}). Longitude represents the position on the East-West axis. Lines of equal longitude are called meridians. The prime meridian is the meridian of value 0\degree, it may change depending on the map. The most used prime meridian is the Greenwich meridian. The longitude corresponds to $\lambda$ in our above example.\\
Historically a position is given with latitude first then longitude with the angles in degrees minutes seconds (sexagesimal base). In web applications, the angles are floats in degrees. Another change of convention between web applications and traditional cartography is the order of the coordinates: the longitude comes first, then the latitude. The reason is that on a map where North is up, longitude is the $x$ axis, and coordinates $(x,y)$ are traditionnaly given with $x$ first in computer science.  

\subsubsection{Web Mercator}

Web Mercator is the commonly used projection in web mapping applications, it resembles the Mercator projection but cuts coverage for the north and south pole at respectively 85.051129\degree   and -85.051129\degree  of latitude. It is special in that it takes the coordinates of WGS 84 which assume that the Earth is an ellipsoid, but projects it as if it were a sphere. With $\lambda$ being the longitude in radians, $\phi$ the latitude in radians, $R$ the radius of the Earth in meters ( 6371007.0 ), the formula for the Web Mercator is the following:
\begin{equation}
\begin{split}
x =& R \lambda  \text{ meters} \\
y =&  R \text{ln} \Big[\text{tan}\big( \dfrac{\pi}{4} + \dfrac{\phi}{2} \big) \Big] \text{ meters}
\end{split}
\label{eq:webmercator}
\end{equation}
This projection, to the contrary of the standard Mercator projection, is slightly non-conformal. Remark that it is also non-equiareal. A surface of one meter square close to the origin point on the projected surface is close to a surface of one meter square in reality, however this is not true for surfaces closer to the poles. The web mercator distorts shapes with a scale factor of approximately $\dfrac{1}{\text{cos}\phi}$. See that this equation uses only one constant for the radius, as if it were a sphere.


\subsubsection{Reprojection}

Once the data is projected in a format using a certain surface parameterization $f: S \rightarrow A$, which is a mapping from the surface of the Earth to a 2D surface, it is possible to switch to another projection $g: S \rightarrow B$ with a function  $t: A \rightarrow B$ such that $t \circ f \equiv g$. \\
In our example of web mercator, $t$ is the function corresponding to equation~\ref{eq:webmercator}, $f$ is not defined here and is a parameterization resembling the sphere's (see equation~\ref{eq:sphere}). Remark that $f \circ t$ in Web Mercator is not a parameterization of the full surface of the Earth, because the poles are cut off. \\
In geospatial engineering,  most of the data is already in WGS 84 (latitude, longitude) which defines a parameterization of the surface of the Earth, this is why we focus on the equation of the reprojections such as in the equation~\ref{eq:webmercator}. Some renderers (e.g openlayers) can reproject on the fly, allowing multiple data sources encoded in different coordinate systems (i.e. projections). 

\subsubsection{From parameterization to pixel coordinates}

Web map applications convert coordinates obtained by a parameterization in pixel coordinates using zoom level functions: $\mathbb{R} \times  \mathbb{R} \rightarrow \mathbb{N} \times \mathbb{N}$. These functions are are usually an affine transformation followed by a rounding of the result, because pixel coordinates are integers. 

In Web Mercator, each zoom level corresponds to a resolution in meter by pixel. The transformation from WGS 84 coordinates to pixel coordinates of a web Mercator map using the composition of equation~\ref{eq:webmercator} with a zoom level function is: 

\begin{equation}
\begin{split}
x_{pixels} =& \dfrac{256}{\pi}2^z(\lambda + \pi) \text{ pixels} \\
y_{pixels} =&  \dfrac{256}{\pi}2^z \Big(\pi - \text{ln} \Big[\text{tan}\big( \dfrac{\pi}{4} + \dfrac{\phi}{2} \big) \Big] \Big) \text{ pixels}
\end{split}
\end{equation}
where $z$ is 0 for the first zoom level function, 1 for the second, etc... \\
Remark that in this case, each zoom level doubles the size of each side of the map. For further explanations refer to section~\ref{sec:raster}.

\subsubsection{Dirty reprojectors}

\paragraph{Theory}
When a rendering library does not support a specific projection, it is possible to manipulate the data to make the visual result look like it is in the specific projection. 
Let $(x_1 ,y_1)$ be coordinates with an associate zoom level function $z_1$. Let $(u_1, v_1) = z_1(x_1, y_1)$ be the pixel coordinates. Now suppose a renderer which only accept another coordinate system, meaning it will use a known zoom level function $z_2$.  As $z_2$ is an affine transformation followed by a rounding we can define a pseudo-inverse function $z_2^{-1}$ which ensures that $z_2^{-1} \circ z_2$ is the identity function but without the same guarantee for  $z_2 \circ z_2^{-1}$. Inputing coordinates $(x_2, y_2) = z_2^{-1}( z_1(x_1, y_1))$ will ensure an equal visual content. \\
\paragraph{Application} This works includes an \href{https://github.com/camptocamp/vector-tiles-demo/}{application} coded with the collaboration of Roman Zoller which shows the synchronization of a map swiss projection with an overlay containing hiking and velo routes, and public transports stops. 

\begin{figure}
  \includegraphics[width=\textwidth]{mobilityTiles}
  \caption{Synchronization of two maps}
Hiking routes in green, velo routes in blue, train stops in red, bus stops in black

  \label{fig:tiles-demo-dezoomed}
\end{figure}

A screenshot of this application can be seen on Figure~\ref{fig:tiles-demo-dezoomed}. It shows on the bottom-left corner a conversion between coordinates unde the mouse cursor, we present the results more clearly on Table~\ref{tab:conversion}. \\
For this application we used the official maps from Switzerland and displayed them using \href{https://leafletjs.com/}{leaflet} in swiss coordinates, an overlay displayed with \href{https://www.mapbox.com/mapbox-gl-js/api/}{mapbox-gl} and a fork of the plugin \href{https://github.com/mapbox/mapbox-gl-leaflet}{mapbox-gl-leaflet}.  The data in the overlay is in swiss projection, it has been scaled and translated to give valid but fake mercator coordinates. Leaflet uses the same calculations to adjust the overlay. \\
This solution allowed us to use features from mapbox-gl and leaflet. The final result does not show visible loss of precision c.f Figure~\ref{fig:precision-tiles} where the velo route matches the route in the background at high level of details.
\begin{table}
\begin{tabular}{|c|c|c|}
\hline
 & xCoord or Longitude & yCoord or latitude \\
\hline
screenCoordinates & 930 & 429 \\
\hline
WGS 84 & 8.575 & 47.103 \\
\hline
WebMercator & 954550 & -5958884 \\
\hline
Swiss Coordinates (LV95) & 2686250 & 1217500 \\
\hline
Fake WebMercator (mapbox-gl) & 2191603 & -4383205 \\
\hline
Fake WGS 84 & 19.68750 & -36.59789\\
\hline
\end{tabular}
\caption{coordinates under the cursor for Figure~\ref{fig:tiles-demo-dezoomed}}
\centering
Screen coordinates are pixel coordinates relative to the top left pixel.
\label{tab:conversion}
\end{table}

\begin{figure}
  \includegraphics[width=\textwidth]{Precision-vector-tiles}
  \caption{Precision of overlay}
  \label{fig:precision-tiles}
\end{figure}

\subsubsection{Projections code}
Projections have an EPSG (European Petroleum Survey Group) code which serves as an identifier to avoid confusion. WebMercator's code is EPSG:3857, WGS84 code is EPSG:4326, the old swiss projection's code is EPSG:21781 and the new swiss projection's code is EPSG:2056. Proj4.js is a library that converts coordinates from one system to another. 